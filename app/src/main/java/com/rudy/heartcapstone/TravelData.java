package com.rudy.heartcapstone;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by Rudy on 2017-01-26.
 */

class TravelData {
    TravelData(Calendar dep, Calendar arr, int dist) {
        departureDate = dep;
        arrivalDate = arr;
        distance = dist;
        duration = TimeUnit.MILLISECONDS.toSeconds(arr.getTimeInMillis() - dep.getTimeInMillis());

        data = new ArrayList<>();
    }

    void addData(int heartrate, int speed, int time) {
        data.add(new tuple<>(heartrate, speed, time));
    }

    Calendar departureDate;
    Calendar arrivalDate;
    int distance; // km/h
    long duration; // seconds
    ArrayList<tuple<Integer, Integer, Integer>> data;
}

class tuple<T, U, V> {
    final T first;
    final U second;
    final V third;

    tuple(T first, U second, V third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }
}

