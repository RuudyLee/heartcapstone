package com.rudy.heartcapstone;

import android.graphics.Color;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;


/**
 * Created by Rudy on 2017-01-25.
 */

public class HeartRateFragment extends Fragment implements View.OnClickListener {
    public static String ARG_DATA = "GIVEMETHOUPOWER";

    private TravelData travelData;
    private GraphView graph;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Generate the TravelData with the received data
        String jsonData = this.getArguments().getString(ARG_DATA);
        travelData = new Gson().fromJson(jsonData, TravelData.class);

        //returning our layout file
        return inflater.inflate(R.layout.fragment_heartrate, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // set button's click action
        Button button = (Button) view.findViewById(R.id.close_button);
        button.setOnClickListener(this);

        // Initialize the graph
        graph = (GraphView) view.findViewById(R.id.graph);
        graph.getViewport().setScrollable(true);

        // set X bounds and make it scrollable
        graph.getViewport().setScalable(true);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(3600); // 1 hour

        // set manual Y bounds
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(200);
        graph.getLegendRenderer().setVisible(true);

        // customize x-axis labels to be time-based
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show as time
                    long val = (long) value;
                    long seconds = val % 60;
                    long minutes = (val / 60) % 60;
                    long hours = (val / 3600) % 24;

                    return String.format("%02d:%02d:%02d", hours, minutes, seconds);
                } else {
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        if (travelData != null) {
            // Generate the graph
            buildGraph();

            // Set the textView values
            TextView departureTextView = (TextView) view.findViewById(R.id.tv_departure);
            TextView arrivalTextView = (TextView) view.findViewById(R.id.tv_arrival);
            TextView distanceTextView = (TextView) view.findViewById(R.id.tv_distance);
            TextView durationTextView = (TextView) view.findViewById(R.id.tv_time);

            DateFormat timeStamp = SimpleDateFormat.getDateTimeInstance();
            String display;

            // departure
            timeStamp.setCalendar(travelData.departureDate);
            display = "Departed at " + timeStamp.format(travelData.departureDate.getTime());
            departureTextView.setText(display);

            // arrival
            timeStamp.setCalendar(travelData.arrivalDate);
            display = "Arrived at " + timeStamp.format(travelData.arrivalDate.getTime());
            arrivalTextView.setText(display);

            // distance
            display = "Distance Travelled: " + String.valueOf(travelData.distance) + " km";
            distanceTextView.setText(display);

            // duration
            long elapsedSeconds = travelData.duration;
            long seconds = elapsedSeconds % 60;
            long minutes = (elapsedSeconds / 60) % 60;
            long hours = (elapsedSeconds / 3600) % 24;
            display = "Time Travelling: " + String.format("%02d:%02d:%02d", hours, minutes, seconds);
            durationTextView.setText(display);

        } else {
            Log.d("TAG", "Travel data is null");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_button:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }

    private void buildGraph() {
        if (travelData.data.size() > 0) {
            // Initialize graph
            LineGraphSeries<DataPoint> heartSeries = new LineGraphSeries<>();
            LineGraphSeries<DataPoint> speedSeries = new LineGraphSeries<>();
            heartSeries.setTitle("Heart Rate");
            speedSeries.setTitle("Speed");
            heartSeries.setColor(Color.RED);
            speedSeries.setColor(Color.BLUE);

            for (tuple<Integer, Integer, Integer> item : travelData.data) {
                double x = item.third; // time
                double heartrate = item.first;  // heartrate
                double speed = item.second; // speed

                heartSeries.appendData(new DataPoint(x, heartrate), true, travelData.data.size());
                speedSeries.appendData(new DataPoint(x, speed), true, travelData.data.size());
            }

            graph.addSeries(heartSeries);
            graph.addSeries(speedSeries);
        }
    }
}