package com.rudy.heartcapstone;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        TravelListFragment.OnItemSelectedListener,
        GoogleApiClient.OnConnectionFailedListener {

    static int RC_SIGN_IN = 135;
    GoogleApiClient mGoogleApiClient;
    private boolean isSignedIn;
    int currentScreen;

    @Override
    public void onItemSelected(String data) {
        // Send data over to a newly created fragment
        Bundle bundle = new Bundle();
        bundle.putString(HeartRateFragment.ARG_DATA, data);
        HeartRateFragment heartRateFragment = new HeartRateFragment();
        heartRateFragment.setArguments(bundle);

        // Replace content_frame with the new fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, heartRateFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Google sign-in configuration
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Navigation drawer
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Set the sign-in button clicker to this class
        View headerLayout = navigationView.getHeaderView(0);
        SignInButton signInButton = (SignInButton) headerLayout.findViewById(R.id.sign_in_button);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.sign_in_button:
                        signIn();
                        break;
                }
            }
        });

        // Check if user is already signed in
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            isSignedIn = true;
            signInButton.setVisibility(View.INVISIBLE);
            displaySelectedScreen(R.id.nav_heartrate);
            navigationView.getMenu().getItem(2).setVisible(true);
        } else {
            displaySelectedScreen(new SignInFragment());
            isSignedIn = false;
        }

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.nav_sign_out) {
            signOut();
        } else {
            displaySelectedScreen(item.getItemId());
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if (status.isSuccess()) {
                            // logout successful, display the correct stuff
                            isSignedIn = false;
                            currentScreen = R.id.nav_sign_out;

                            // Navigation drawer
                            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

                            // Set the sign-in button to visible
                            View headerLayout = navigationView.getHeaderView(0);
                            SignInButton signInButton = (SignInButton) headerLayout.findViewById(R.id.sign_in_button);
                            signInButton.setVisibility(View.VISIBLE);

                            // Set the sign-out button to invisible
                            navigationView.getMenu().getItem(2).setVisible(false);

                            // Return to sign in fragment
                            displaySelectedScreen(new SignInFragment());
                        }
                    }
                }
        );
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("MainActivity", "handleSignInResult: " + result.isSuccess());

        if (result.isSuccess()) {
            // sign in successful
            isSignedIn = true;
            SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
            signInButton.setVisibility(View.INVISIBLE);
            displaySelectedScreen(R.id.nav_heartrate);

            // display the sign out button
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.getMenu().getItem(2).setVisible(true);
        }
    }

    private void displaySelectedScreen(int itemId) {
        if (isSignedIn && currentScreen != itemId) {
            Fragment fragment = null;

            // initialize the selected fragment
            switch (itemId) {
                case R.id.nav_heartrate:
                    fragment = new TravelListFragment();
                    break;
                case R.id.nav_sleep:
                    fragment = new SleepListFragment();
                    break;
            }

            // replace the fragment
            if (fragment != null) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.commit();

                // Highlight the item in the navigation pane
                currentScreen = itemId;
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                navigationView.setCheckedItem(itemId);
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void displaySelectedScreen(Fragment frag) {
        if (frag != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, frag);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
}
