package com.rudy.heartcapstone;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * Created by Rudy on 2017-01-26.
 */

public class TravelListFragment extends ListFragment implements AdapterView.OnItemClickListener {
    String filename = "username.json";

    OnItemSelectedListener mCallback;

    private ArrayAdapter adapter;
    private ArrayList<TravelData> dataFromFile;

    /*
    For communication between fragment and activity
     */
    public interface OnItemSelectedListener {
        void onItemSelected(String data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented the callback interface
        // If not, it throws an exception
        try {
            mCallback = (OnItemSelectedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnItemSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_travel_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // FOR TESTING PURPOSES, GENERATING A BUNCH OF FAKE DATA
        File file = new File(filename);
        file.delete();
        generateRandomData(10);

        // READ THE JSON DATA WE JUST SAVED
        dataFromFile = unserialize(filename);


        adapter = new TravelListAdapter(getActivity(), dataFromFile);
        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);
    }

    /*
    For testing purposes
     */
    private void generateRandomData(int numItems) {
        ArrayList<TravelData> arrayList = new ArrayList<>();

        for (int i = 0; i < numItems; i++) {
            Calendar departureTime = new GregorianCalendar();
            departureTime.add(Calendar.HOUR, -i * 4);
            Calendar arrivalTime = new GregorianCalendar();
            arrivalTime.setTime(departureTime.getTime());
            arrivalTime.add(Calendar.SECOND, new Random().nextInt((18000 - 600) + 1) + 600);
            int distanceTravelled = new Random().nextInt((200 - 10) + 1) + 10;
            TravelData travelData = new TravelData(departureTime, arrivalTime, distanceTravelled);
            int heartrate = new Random().nextInt((80 - 70) + 1) + 70;
            int speed = new Random().nextInt((100 - 40) + 1) + 40;
            for (int j = 0; j < travelData.duration; j += 10) {
                heartrate += new Random().nextInt((1 + 1) + 1) - 1;
                speed += new Random().nextInt((1 + 1) + 1) - 1;
                travelData.addData(heartrate, speed, j);
            }
            arrayList.add(travelData);
        }

        // Convert array list to json
        Gson gson = new Gson();
        String s = gson.toJson(arrayList);

        // Save the json to file
        FileOutputStream outputStream;
        try {
            outputStream = getContext().openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(s.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    Unserializes the data in the json file
     */
    private ArrayList<TravelData> unserialize(String fn) {
        ArrayList<TravelData> tempArray = new ArrayList<>();

        try {
            FileInputStream fis = getContext().openFileInput(fn);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            // Convert from json to ArrayList<TravelData>
            String json = sb.toString();
            tempArray = new Gson().fromJson(json, new TypeToken<ArrayList<TravelData>>() {
            }.getType());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tempArray;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Send the data from the clicked item over to the activity in json format
        String data = new Gson().toJson(dataFromFile.get(position));
        mCallback.onItemSelected(data);
    }
}
