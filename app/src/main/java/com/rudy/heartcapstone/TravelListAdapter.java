package com.rudy.heartcapstone;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Rudy on 2017-01-26.
 */

class TravelListAdapter extends ArrayAdapter<TravelData> {
    private Context context;

    TravelListAdapter(Context context, List<TravelData> items) {
        super(context, android.R.layout.simple_list_item_1, items);
        this.context = context;
    }

    // Holder for the list items
    private class ViewHolder {
        TextView departureText;
        TextView arrivalText;
        TextView distanceText;
        TextView timeText;
        GraphView graphView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View viewToUse;

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            viewToUse = mInflater.inflate(R.layout.travel_list_item, null);

            holder = new ViewHolder();
            holder.departureText = (TextView) viewToUse.findViewById(R.id.tv_departure);
            holder.arrivalText = (TextView) viewToUse.findViewById(R.id.tv_arrival);
            holder.distanceText = (TextView) viewToUse.findViewById(R.id.tv_distance);
            holder.timeText = (TextView) viewToUse.findViewById(R.id.tv_time);
            holder.graphView = (GraphView) viewToUse.findViewById(R.id.graphView);
            holder.graphView.getViewport().setScalable(true);
            holder.graphView.getViewport().setScrollable(true);
            holder.graphView.getViewport().setMinX(0);
            holder.graphView.getViewport().setMaxX(3600); // 1 hour
            holder.graphView.getViewport().setYAxisBoundsManual(true);
            holder.graphView.getViewport().setMinY(0);
            holder.graphView.getViewport().setMaxY(200);
            holder.graphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
                @Override
                public String formatLabel(double value, boolean isValueX) {
                    if (isValueX) {
                        // show as time
                        long val = (long) value;
                        long seconds = val % 60;
                        long minutes = (val / 60) % 60;
                        long hours = (val / 3600) % 24;

                        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
                    } else {
                        return super.formatLabel(value, isValueX);
                    }
                }
            });

            viewToUse.setTag(holder);
        } else {
            viewToUse = convertView;
            holder = (ViewHolder) viewToUse.getTag();
        }

        // Generate the list item
        TravelData travelData = getItem(position);

        // Text boxes
        DateFormat timeStamp = SimpleDateFormat.getDateTimeInstance();
        Calendar departureCalendar = travelData.departureDate;
        timeStamp.setCalendar(departureCalendar);
        String dep = "dep. " + timeStamp.format(departureCalendar.getTime());
        holder.departureText.setText(dep);

        Calendar arrivalCalendar = travelData.arrivalDate;
        timeStamp.setCalendar(arrivalCalendar);
        String arr = "arr.   " + timeStamp.format(arrivalCalendar.getTime());
        holder.arrivalText.setText(arr);

        String dist = "dist. " + String.valueOf(travelData.distance) + " km";
        holder.distanceText.setText(dist);

        long elapsedSeconds = travelData.duration;
        long seconds = elapsedSeconds % 60;
        long minutes = (elapsedSeconds / 60) % 60;
        long hours = (elapsedSeconds / 3600) % 24;
        String dur = "dur.  " + String.format("%02d:%02d:%02d", hours, minutes, seconds);
        holder.timeText.setText(dur);

        // build the graph
        holder.graphView.removeAllSeries();
        buildGraph(travelData, holder.graphView);

        return viewToUse;
    }

    /*
    Builds a graph based on given data
     */
    private void buildGraph(TravelData travelData, GraphView graphView) {
        if (travelData.data.size() > 0) {
            // Initialize graph
            LineGraphSeries<DataPoint> heartSeries = new LineGraphSeries<>();
            LineGraphSeries<DataPoint> speedSeries = new LineGraphSeries<>();
            heartSeries.setColor(Color.RED);
            speedSeries.setColor(Color.BLUE);

            for (tuple<Integer, Integer, Integer> item : travelData.data) {
                double x = item.third; // time
                double heartrate = item.first;  // heartrate
                double speed = item.second; // speed

                heartSeries.appendData(new DataPoint(x, heartrate), true, travelData.data.size());
                speedSeries.appendData(new DataPoint(x, speed), true, travelData.data.size());
            }

            graphView.addSeries(heartSeries);
            graphView.addSeries(speedSeries);
        }
    }
}
