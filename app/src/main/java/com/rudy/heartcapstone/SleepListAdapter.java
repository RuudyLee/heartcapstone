package com.rudy.heartcapstone;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Rudy on 2017-01-26.
 */

public class SleepListAdapter extends ArrayAdapter<SleepDataItem> {
    private Context context;
    private int optimalSleep;

    SleepListAdapter(Context context, List<SleepDataItem> items) {
        super(context, android.R.layout.simple_list_item_1, items);
        this.context = context;
        optimalSleep = 8 * 60 * 60; // 8 hours
    }

    // Holder for the list items
    private class ViewHolder {
        TextView dateText;
        TextView dayOfWeekText;
        TextView sleepDurationText;
        TextView sleepDeficitText;
        TextView deepSleepCycleText;
        TextView noiseText;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SleepListAdapter.ViewHolder holder;
        View viewToUse;

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            viewToUse = mInflater.inflate(R.layout.sleep_list_item, null);

            holder = new SleepListAdapter.ViewHolder();
            holder.dateText = (TextView) viewToUse.findViewById(R.id.tv_date);
            holder.dayOfWeekText = (TextView) viewToUse.findViewById(R.id.tv_day_of_week);
            holder.sleepDurationText = (TextView) viewToUse.findViewById(R.id.tv_sleep_duration);
            holder.sleepDeficitText = (TextView) viewToUse.findViewById(R.id.tv_sleep_deficit);
            holder.deepSleepCycleText = (TextView) viewToUse.findViewById(R.id.tv_dsp);
            holder.noiseText = (TextView) viewToUse.findViewById(R.id.tv_noise);
            viewToUse.setTag(holder);
        } else {
            viewToUse = convertView;
            holder = (SleepListAdapter.ViewHolder) viewToUse.getTag();
        }

        // Generate the list item
        String display;
        SleepDataItem sleepDataItem = getItem(position);

        // Sleep Start
        DateFormat dateFormat = SimpleDateFormat.getDateInstance();
        display = dateFormat.format(sleepDataItem.sleepStart.getTime());
        holder.dateText.setText(display);
        holder.dayOfWeekText.setText(new SimpleDateFormat("EEEE").format(sleepDataItem.sleepStart.getTime()));

        // Sleep Duration
        long sleepDuration = sleepDataItem.sleepDuration;
        long minutes = (sleepDuration / 60) % 60;
        long hours = (sleepDuration / 3600) % 24;
        display = String.format("%02d:%02d", hours, minutes);
        holder.sleepDurationText.setText(display);

        // Sleep Deficit
        long difference;
        if (sleepDuration > optimalSleep) {
            difference = sleepDuration - optimalSleep;
            minutes = (difference / 60) % 60;
            hours = (difference / 3600) % 24;
            display = String.format("%02d:%02d", hours, minutes);
            holder.sleepDeficitText.setTextColor(Color.GREEN);
        } else {
            difference = optimalSleep - sleepDuration;
            minutes = (difference / 60) % 60;
            hours = (difference / 3600) % 24;
            display = String.format("-%02d:%02d", hours, minutes);
            holder.sleepDeficitText.setTextColor(Color.RED);
        }
        holder.sleepDeficitText.setText(display);

        // Deep Sleep Cycle
        if (sleepDataItem.deepSleepPercentage < 25) {
            holder.deepSleepCycleText.setTextColor(Color.RED);
        } else {
            holder.deepSleepCycleText.setTextColor(Color.GREEN);
        }
        display = String.valueOf(sleepDataItem.deepSleepPercentage) + "%";
        holder.deepSleepCycleText.setText(display);

        // Noise
        display = String.valueOf(sleepDataItem.noisePercentage) + "%";
        holder.noiseText.setText(display);

        return viewToUse;
    }
}
