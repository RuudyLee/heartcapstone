package com.rudy.heartcapstone;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by Rudy on 2017-01-26.
 */

public class SleepDataItem {

    SleepDataItem(Calendar start, Calendar end, int dsp, int np) {
        sleepStart = start;
        sleepEnd = end;
        deepSleepPercentage = dsp;
        noisePercentage = np;

        sleepDuration = TimeUnit.MILLISECONDS.toSeconds(end.getTimeInMillis() - start.getTimeInMillis());
    }

    Calendar sleepStart;
    Calendar sleepEnd;
    long sleepDuration;
    int deepSleepPercentage;
    int noisePercentage;

}
