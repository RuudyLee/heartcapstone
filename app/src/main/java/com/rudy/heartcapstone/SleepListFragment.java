package com.rudy.heartcapstone;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * Created by Rudy on 2017-01-26.
 */

public class SleepListFragment extends ListFragment {
    String filename = "sleep.json";
    private int optimalSleep;

    private ArrayAdapter adapter;
    private ArrayList<SleepDataItem> dataFromFile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sleep_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // FOR TESTING PURPOSES, GENERATING A BUNCH OF FAKE DATA
        Log.d("TAG", "OHLORD");

        dataFromFile = new ArrayList<>();
        // READ THE JSON DATA WE JUST SAVED
        for (int i = 0; i < 30; i++) {
            Calendar sleepStart = new GregorianCalendar();
            sleepStart.add(Calendar.DAY_OF_MONTH, -i);
            Calendar sleepEnd = new GregorianCalendar();
            sleepEnd.setTime(sleepStart.getTime());
            sleepEnd.add(Calendar.MINUTE, new Random().nextInt((600 - 300) + 1) + 300);
            int dsp = new Random().nextInt((60 - 10) + 1) + 10;
            int noise = new Random().nextInt((30 - 5) + 1) + 5;
            SleepDataItem sleepDataItem = new SleepDataItem(sleepStart, sleepEnd, dsp, noise);
            dataFromFile.add(sleepDataItem);
        }

        optimalSleep = 8 * 60 * 60;

        // Get the total sleep deficit
        int totalDeficit = 0;
        for (SleepDataItem item : dataFromFile) {
            totalDeficit += item.sleepDuration - optimalSleep;
        }

        TextView deficitText = (TextView) getView().findViewById(R.id.tv_total_deficit);
        String display;
        if (totalDeficit < 0) {
            deficitText.setTextColor(Color.RED);
            totalDeficit *= -1;
            long minutes = (totalDeficit / 60) % 60;
            long hours = (totalDeficit / 3600) % 24;
            display = String.format("-%02d:%02d", hours, minutes);
        } else {
            deficitText.setTextColor(Color.GREEN);
            long minutes = (totalDeficit / 60) % 60;
            long hours = (totalDeficit / 3600) % 24;
            display = String.format("%02d:%02d", hours, minutes);
        }
        deficitText.setText(display);


        adapter = new SleepListAdapter(getActivity(), dataFromFile);
        setListAdapter(adapter);
    }

}
